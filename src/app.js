var path = require('path');
var express = require('express');
var morgan = require('morgan');
var app = express();
var mongoose = require('mongoose');

//importing routes
var indexRoutes = require('./routes/index');
var bicicletasRoutes = require('./routes/bicicletas');
var bicicletasAPIRoutes = require('./routes/api/bicicletas');

//conecting to db

//mongoose.connect('mongodb://localhost/');

//settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname + '/views'));
app.set('view engine', 'pug');

//middelwares
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));


//routes
app.use('/', indexRoutes);
app.use('/bicicletas', bicicletasRoutes);
app.use('/api/bicicletas', bicicletasAPIRoutes);

//starting the server
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});
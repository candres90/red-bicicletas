var Bicicleta  = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function(){
    return `id: ${this.id} | color: ${this.color}`;
};

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
};

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(item => item.id == aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error(`No existe bicicleta con el ID: ${aBici}`);
};


Bicicleta.removeById = function(aBiciId){
    for(var i=0; i < Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
};


Bicicleta.add(new Bicicleta(1,'Rojo','Urbana',[4.65429,-74.10076]));
Bicicleta.add(new Bicicleta(2,'Amarillo','Urbana',[4.65305,-74.10158]));
Bicicleta.add(new Bicicleta(3,'Gris','Urbana',[4.65203,-74.10194]));

module.exports = Bicicleta;